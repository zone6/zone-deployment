# Deployment of Zone Services

Follwoing figure discussed the architecture of the Zone platfrom. 

![Alt text](zone-architecture.jpg?raw=true "Zone platfrom architecture")

## Services

There are three main servies;

```
1. elassandra
2. aplos
3. gateway
```

## Configuration

Change `host.docker.local` field in `.env` file to local machines ip. also its possible to add a host entry to `/etc/hosts` file by overriding `host.docker.local` with local machines ip. following is an example of `/etc/hosts` file.

```
20.114.85.104    host.docker.local
```

## Deploy services

Start services in following order;

```
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d web
```
** After hosting website, it can be reached on `<Ip address of the Vm>:4600`.


## Connect apis

gateway service will start a REST api on `7654` port. For an example if your machines ip is `20.114.85.104` the apis can be access via `20.114.85.104:4600/api/<apiname>`.Following are the available rest api end points and their specifications,

** Open `7654` and `4600` port on VM for public

#### 1. PUT Medical Record

```
# request
curl -XPOST "http://localhost:7654/api/record" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "111110",
  "execer": "admin:admin",
  "messageType": "addRecord",
  "userMobile": "0702219595",
  "userName": "Udith Jayasinghe",
  "recordStatus": "done"
}
'

# reply
{"code":200,"msg":"record added"}
```


#### 2. GET Medical Record 

```
# request
curl -XPOST "http://localhost:7654/api/record" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "1111xx",
  "execer": "admin:admin",
  "messageType": "getRecord",
  "userMobile": "0702219595",
}
'

# reply
{"userMobile":"0702219595","userName":"Udith Jayasinghe","recordStatus": "done", "timestamp":"2021-07-12 20:55:21.308"}
```
